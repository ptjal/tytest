﻿/* Copyright © 2017 Tylor Allison */

using System;
using System.Collections.Generic;
using UnityEngine;

namespace TyTest {

    public class GenericTest : ITestModule {
        public List<GameObject> objectCache;

        public GenericTest() {
            objectCache = new List<GameObject>();
        }
        protected TestRunner runner;
        public virtual void Init(TestRunner runner) {
            this.runner = runner;
        }
        public virtual void Update() {
        }
        public virtual void Start() {
        }
        public virtual void Stop() {
            ClearCache();
        }
        protected void ClearCache() {
            foreach (var go in objectCache) {
                UnityEngine.Object.Destroy(go);
            }
            objectCache.Clear();
        }
        public virtual void Restart() {
            Stop();
            Start();
        }
    }

    public class GenericTestWithConfig<T> : GenericTest where T: TestConfig {
        protected static T config;
        public override void Init(TestRunner runner) {
            base.Init(runner);
            LoadConfig();
        }

        protected virtual void LoadConfig() {
            config = TestConfig.Load<T>(runner.objectCache, OnConfigChange);
        }

        protected virtual void OnConfigChange() {
            Restart();
        }

    }
}
