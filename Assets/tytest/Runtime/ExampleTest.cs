﻿/* Copyright © 2017 Tylor Allison */

using System;
using UnityEngine;

namespace TyTest {


    public class ExampleConfig: TestConfig {
        // public config values
        [Range (0,4)]
        public int value1;
    }

    public class ExampleTest : GenericTestWithConfig<ExampleConfig> {
        static ExampleTest() {
            Debug.Log("ExampleTest.static constructor");
        }

        public override void Start() {
            Debug.Log("ExampleTest.Start");
        }

    }
}
