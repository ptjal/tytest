﻿/* Copyright © 2017 Tylor Allison */

using UnityEngine;
using UnityEditor;
using System;
using System.Text;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace TyTest {

    public class TestRunner : MonoBehaviour {
        public string testChoice;
        public List<GameObject> objectCache;
        ITestModule currentTest;
        bool running = false;

        void OnValidate() {
            if (Application.isPlaying && running) {
                StartCoroutine(SwitchTest());
            }
        }

        IEnumerator SwitchTest() {
            // wait a frame
            yield return null;
            // stop current test
            StopTest();
            // start new test
            StartTest();
        }

        public static ITestModule GetTestModule(string testChoice) {
            System.Reflection.Assembly[] assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
            foreach (var assembly in assemblies) {
                try {
                    var targetType = assembly.GetType("TyTest." + testChoice);
                    //var testCase = Activator.CreateInstance(targetType).Unwrap();
                    if (targetType != null) {
                        var testCase = Activator.CreateInstance(targetType);
                        return (ITestModule) testCase;
                    }
                }
                catch (TypeLoadException){
                    //Debug.Log("Failed to load test: " + testChoice + " from assembly: " + assembly);
                }
            }
            Debug.Log("Failed to load test: " + testChoice);
            return null;
        }

        void StartTest() {
            objectCache = new List<GameObject>();
            currentTest = GetTestModule(testChoice);
            if (currentTest != null) {
                currentTest.Init(this);
                currentTest.Start();
            }
        }

        void StopTest() {
            if (currentTest != null) {
                currentTest.Stop();
            }
            foreach (var go in objectCache) {
                Destroy(go);
            }
            objectCache = null;
            currentTest = null;
        }

        void Start () {
            // start the selected test
            StartTest();
            running = true;
        }

        void OnDrawGizmos() {
            /*
            if (debugMeshData != null) {
                var mesh = debugMeshData.ConvertMesh();
                Gizmos.DrawWireMesh(mesh, new Vector3(0,5,0));
            }
            */
        }

    }

}
