﻿/* Copyright © 2017-2018 Tylor Allison */

using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

namespace TyTest {

    public class TestConfig : MonoBehaviour {

        [HideInInspector]
        public UnityEvent onModifyEvent;

        bool triggerModify;
        bool firstTime = true;

        void Awake() {
            onModifyEvent = new UnityEvent();
            triggerModify = false;
        }

        void OnValidate() {
            // OnValidate is called during start up, but we don't want to trigger a restart during start, don't raise
            // onModifyEvent first time through
            if (!firstTime) {
                triggerModify = true;
            }
            firstTime = false;
        }

        void Update() {
            if (triggerModify) {
                triggerModify = false;
                onModifyEvent.Invoke();
            }
        }

        public static T Load<T>(
            List<GameObject> objectCache,
            UnityAction onModify
        ) where T:TestConfig {
            var go = new GameObject(typeof(T).ToString());
            if (objectCache != null) {
                objectCache.Add(go);
            }

            // add config as component of config GO
            var config = (T) go.AddComponent(typeof(T));
            if (onModify != null) {
                config.onModifyEvent.AddListener(onModify);
            }

            return config;
        }
    }
}
