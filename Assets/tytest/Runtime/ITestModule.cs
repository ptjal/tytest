﻿/* Copyright © 2017 Tylor Allison */

namespace TyTest {
    public interface ITestModule {
        void Init(TestRunner runner);
        void Update();
        void Start();
        void Stop();
    }
}
