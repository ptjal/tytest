# tytest - a Unity package providing a simple to use test framework

## Design

This module provides a simple plugable test framework that can be used in test scenes to define tests for your different features.  The concept is that there is a single test runner that discovers all test classes defined in a project.  Each test class consists of set of methods specific for the feature being tested.  The test class interface includes a Start, Stop, and Update method.  Start is executed when the test is instantiated by the test runner, and should be overridden to include the gameobject or other setup required to run the test.  Stop is executed when the test is stopped by the runner.  This should include any test-specific clean up.  Update is called per-frame from the runner, which is a Unity MonoBehaviour script.

Two base test classes are provided, a GenericTest which provides the test interface as decribed
above, and a GenericTestWithConfig<T> which provides the same test interface but with links to
user-specified test configuration.  The test configuration must be derived from TestConfig and
provides a mechanism to specify test parameters for the test case.  The test config is displayed
in the editor which allows for the test to be tweaked on-the-fly.  Any changes to the test
config cause the test case to be restarted.

## Usage

Add the TestRunner to any Unity GameObject.  The TestRunner script will show up in the editor
along with public variables controlling which test case to run.  Test cases are discovered
automatically.

Define test cases from the **GenericTest** or **GenericTestWithConfig** classes and implement
the **Start**, **Update**, and **Stop** methods as needed.  ***NOTE: Test cases MUST be defined
in the TyTest namespace, else discovery will not work***.

Start the Unity Editor, select desired test and modify config as needed.
