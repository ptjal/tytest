﻿/* Copyright © 2017 Tylor Allison */

using UnityEngine;
using UnityEditor;
using System;
using System.Text;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace TyTest {

    [CustomEditor(typeof(TestRunner), true)]
    [CanEditMultipleObjects]
    public class TestRunnerEditor : Editor {
        SerializedProperty testChoiceProp;
        int testChoiceIndex = 0;

        public static System.Type[] GetAllSubTypes(System.Type aBaseClass) {
            var result = new System.Collections.Generic.List<System.Type>();
            System.Reflection.Assembly[] AS = System.AppDomain.CurrentDomain.GetAssemblies();
            foreach (var A in AS) {
                System.Type[] types = A.GetTypes();
                foreach (var T in types) {
                    if (T.IsSubclassOf(aBaseClass)) {
                        result.Add(T);
                    }
                }
            }
            return result.ToArray();
        }

        private string[] GetTestChoices() {
            var testChoices = new List<string>();
            testChoices.Add("NoTest");
            Type[] typelist = GetAllSubTypes(typeof(GenericTest));
            for (int i = 0; i < typelist.Length; i++) {
                // exclude generic subtype
                if (typelist[i].Name != "GenericTestWithConfig`1") {
                    testChoices.Add(typelist[i].Name);
                }
            }
            return testChoices.ToArray();
        }

        void OnEnable() {
            // Setup the SerializedProperties.
            testChoiceProp = serializedObject.FindProperty("testChoice");
            // Set the choice index to the previously selected index
            var testChoices = GetTestChoices();
            testChoiceIndex = Array.IndexOf(testChoices, testChoiceProp.stringValue);
        }

        public override void OnInspectorGUI() {
            // Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
            serializedObject.Update();

            //doing the orientation thing
            var testChoices = GetTestChoices();
            testChoiceIndex = EditorGUILayout.Popup("Test Choice", testChoiceIndex, testChoices);
            if (testChoiceIndex < 0)
                testChoiceIndex = 0;
            testChoiceProp.stringValue = testChoices[testChoiceIndex];

            // Apply changes to the serializedProperty - always do this in the end of OnInspectorGUI.
            serializedObject.ApplyModifiedProperties();
        }
    }
}
